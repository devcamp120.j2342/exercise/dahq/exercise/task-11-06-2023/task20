package com.devcamp.stringapi.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class StringService {
    public String daoNguocChuoi(String paramString) {
        String re = "";
        for (int i = paramString.length() - 1; i >= 0; i--) {
            re += paramString.charAt(i);
        }
        return re;
    }

    public boolean chuoiPalindrom(String paramString) {
        String re = "";
        for (int i = paramString.length() - 1; i >= 0; i--) {
            re += paramString.charAt(i);
        }
        if (paramString.equals(re)) {
            return true;
        }
        return false;
    }

    public String Task3(String paramString) {

        StringBuilder result = new StringBuilder();
        Set<Character> seen = new HashSet<>();

        for (char ch : paramString.toCharArray()) {
            if (seen.add(ch)) {
                result.append(ch);
            }
        }
        return result.toString();
    }

    public String Task4(String str1, String str2) {
        String re = "";
        if (str1.length() > str2.length()) {
            String subStr1 = str1.substring(str1.length() - str2.length());
            re = subStr1 + str2;
        } else if (str1.length() < str2.length()) {
            String subStr2 = str2.substring(str2.length() - str1.length());
            re = str1 + subStr2;
        } else {
            re = str1 + str2;
        }
        return re;
    }
}